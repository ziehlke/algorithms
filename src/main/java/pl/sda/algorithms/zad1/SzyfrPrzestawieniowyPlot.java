package pl.sda.algorithms.zad1;

import java.util.ArrayList;
import java.util.List;

public class SzyfrPrzestawieniowyPlot {

    public String zaszyfruj(String tekst) {
        // zamieniamy tekst na ciąg znaków
        char[] tablicaZnakow = tekst.toCharArray();

        String wynik = "";

        // iterujemy co drugi znak (zaczynając od 0go)
        // i przepisujemy wszystkie znaki do [listy/tablicy/string'a]
        for (int i = 0; i < tablicaZnakow.length; i += 2) { // przeskok o 2 ponieważ i+=2
            wynik += tablicaZnakow[i];
        }

        // iterujemy co drugi znak (zaczynając od 1go)
        // i przepisujemy (dopisujemy) wszystkie znaki do [listy/tablicy/string'a]
        for (int i = 1; i < tablicaZnakow.length; i += 2) { // przeskok o 2 ponieważ i+=2
            wynik += tablicaZnakow[i];
        }

        // jeśli użyliśmy tablicy lub listy, to dodajemy jej elementy do string'a
        // zwracamy string wynikowy
        return wynik;
    }


    public static String odszyfruj(String szyfr) {

        String polowa1, polowa2;

        // dzielę sobie tekst na dwie połowy
        if (szyfr.length() % 2 == 1) {
            // dla nieparzystej liczby elementów połowy wyglądają inaczej
            // linia podziału się przesuwa o 1 element w prawo
            polowa1 = szyfr.substring(0, (szyfr.length() / 2) + 1);
            polowa2 = szyfr.substring((szyfr.length() / 2) + 1, szyfr.length());
        } else {
            polowa1 = szyfr.substring(0, (szyfr.length() / 2));
            polowa2 = szyfr.substring((szyfr.length() / 2), szyfr.length());
        }

        String wynik = "";

        //wypisuje wszystkie znaki ciągu pierwszego
        //ciąg 1 na pewno jest dłuższy
        // dopóki jest jakiś element w ciągu 2, to wypisuje (przeplatając) element ciągu 2
        for (int i = 0; i < polowa1.length(); i++) {
            wynik += polowa1.charAt(i);
            if (polowa2.length() >= i) {
                wynik += polowa2.charAt(i);
            }
        }

        return wynik;
    }

    public static void main(String[] args) {
        System.out.println(odszyfruj("krousr"));
    }
}
